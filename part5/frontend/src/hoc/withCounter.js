import React from 'react';

const withCounter = Component => {
  class WrappedComponent extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        count: 0
      };
    }

    updateCount = () => {
      this.setState(state => {
        return {
          count: state.count + 1
        };
      });
    };
    render() {
      return (
        <Component
          {...this.props}
          count={this.state.count}
          updateCount={this.updateCount}
        />
      );
    }
  }

  return WrappedComponent;
};

export default withCounter;
