import React from 'react';

interface Iprops {
  onProjectFilter: (e: string) => void;
}
class ProjectFilter extends React.Component<Iprops> {
  constructor(props: any) {
    super(props);
    this.onInputChange = this.onInputChange.bind(this);
  }
  onInputChange(e: any) {
    this.props.onProjectFilter(e.target.value);
  }
  render() {
    return (
      <input
        onChange={this.onInputChange}
        className="form-control mt-2 p-3"
        placeholder="enter project name"
      />
    );
  }
}

export default ProjectFilter;
