import React from 'react';
import { Project } from './projectTable';
import ProjectForm from './projectForm';

interface Iprops {
  onAddClick: (project: Project) => void;
}

interface IState {
  formShown: boolean;
}

class ProjectAdd extends React.Component<Iprops, IState> {
  constructor(props: Iprops) {
    super(props);
    this.state = {
      formShown: false
    };
  }
  onAddBtnClicked = () => {
    this.setState(state => {
      return {
        formShown: !state.formShown
      };
    });
  };
  render() {
    const { formShown } = this.state;
    return (
      <div className="row">
        <div className="col-lg-2 m-3">
          <button onClick={this.onAddBtnClicked} className="btn btn-danger ">
            ADD PROJECT
          </button>
          {formShown ? (
            <ProjectForm onAddClick={this.props.onAddClick} />
          ) : null}
        </div>
      </div>
    );
  }
}

export default ProjectAdd;
