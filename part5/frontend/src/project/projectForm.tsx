import React from 'react';
import { Project } from './projectTable';
interface Iprops {
  onAddClick: (project: Project) => void;
}
class ProjectForm extends React.Component<Iprops> {
  state = {
    formData: {
      id: 0,
      name: '',
      description: '',
      owner: '',
      type: 'scrum'
    },
    error: null
  };

  handleInputChange = (event: any) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    if (/\d/.test(value)) {
      this.setState({
        error: 'dont type number'
      });
    } else {
      this.setState({
        error: null
      });
    }
    this.setState({
      formData: {
        ...this.state.formData,
        [name]: value
      }
    });
  };
  handleSubmit = (event: any) => {
    event.preventDefault();
    let newProject: any = {
      ...this.state.formData,
      id: Math.floor(Math.random() * 100 + 1)
    };
    this.props.onAddClick(newProject);
  };
  render() {
    const { error } = this.state;
    return (
      <form className="border border-dark p-3" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            className="form-control"
            name="name"
            value={this.state.formData.name}
            onChange={this.handleInputChange}
            placeholder="Enter name of project"
          />
          {error && <span>{error}</span>}
        </div>

        <div className="form-group">
          <label htmlFor="name">Description</label>
          <input
            type="text"
            className="form-control"
            name="description"
            value={this.state.formData.description}
            onChange={this.handleInputChange}
            placeholder="Enter description of project"
          />
        </div>

        <div className="form-group">
          <label htmlFor="name">Owner</label>
          <input
            type="text"
            className="form-control"
            name="owner"
            value={this.state.formData.owner}
            onChange={this.handleInputChange}
            placeholder="Enter owner of project"
          />
        </div>
        <div className="form-group">
          <label htmlFor="type">Select project type</label>
          <select
            className="form-control"
            name="type"
            value={this.state.formData.type}
            onChange={this.handleInputChange}
          >
            <option value="scrum">scrum</option>
            <option value="kanban">kanban</option>
          </select>
        </div>

        <input type="submit" className="btn btn-dark" value="Submit" />
      </form>
    );
  }
}

export default ProjectForm;
