import React from 'react';
import scrumImage from '../assets//images/scrum.png';
import KanBanImage from '../assets//images/kanban.png';

interface Iprops {
  type: string | boolean;
  width: number;
  height: number;
  onProjectTypeClicked: (type: string | boolean) => void;
}
class ProjectType extends React.Component<{
  type: any;
  width: any;
  height: any;
  onProjectTypeClicked: (type: any) => void;
}> {
  render() {
    const { type, width, height, onProjectTypeClicked } = this.props;
    return type === 'scrum' ? (
      <img
        onClick={e => {
          e.stopPropagation();
          onProjectTypeClicked(type);
        }}
        width={width}
        alt={'scrum'}
        height={height}
        src={scrumImage}
      />
    ) : (
      <img
        onClick={e => {
          e.stopPropagation();
          onProjectTypeClicked(type);
        }}
        width={width}
        alt={'kanban'}
        height={height}
        src={KanBanImage}
      />
    );
  }
}

export default ProjectType;
