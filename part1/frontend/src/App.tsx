import React from 'react';
import './App.css';
import ProjectTable from './project/projectTable';

const App: React.FC = () => {
  return <ProjectTable />;
};

export default App;
