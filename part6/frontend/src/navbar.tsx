import React from 'react';
import { Link } from 'react-router-dom';

const NavBar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <Link to="/project" className="navbar-brand">
        Projects
      </Link>
      <Link to="/user" className="navbar-brand">
        Users
      </Link>
      <Link to="/home" className="navbar-brand">
        Home
      </Link>
    </nav>
  );
};

export default NavBar;
