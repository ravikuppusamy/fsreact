import React from 'react';
import { Project } from './projectTable';
import ProjectType from './projectType';
import { Link } from 'react-router-dom';

interface IProps {
  tableData: Project[];
  onRowClicked: (p: Project) => void;
  selectedProject: Project;
  updateProjectType: (type: any, p: Project) => void;
  onRowDeleteClicked: (e: React.MouseEvent, project: Project) => void;
}
class ProjectTableBody extends React.Component<IProps> {
  onProjectTypeClicked = (type: any, projectToUpdate: Project) => {
    this.props.updateProjectType(type, projectToUpdate);
  };

  renderTds(project: Project) {
    var keys = Object.keys(project);
    const tds = keys.map((key: any) => {
      if (key === 'type') {
        return (
          <td key={key}>
            <ProjectType
              width={50}
              height={50}
              type={project[key]}
              onProjectTypeClicked={key =>
                this.onProjectTypeClicked(key, project)
              }
            />
          </td>
        );
      }
      return <td key={key}>{project[`${key}`]}</td>;
    });

    return [
      ...tds,
      <td key={project.id + 'del'}>
        <button
          onClick={(e: React.MouseEvent) =>
            this.props.onRowDeleteClicked(e, project)
          }
          className="btn btn-danger"
        >
          DELETE
        </button>
        <button className="btn btn-primary ml-2">
          <Link to={'/project/' + project.id} className="navbar-brand">
            VIEW
          </Link>
        </button>
      </td>
    ];
  }

  onRowClicked = (clickedProject: Project) => {
    this.props.onRowClicked(clickedProject);
  };
  renderRows() {
    const trs = this.props.tableData.map(project => {
      let styles = {
        backgroundColor:
          this.props.selectedProject.id === project.id ? 'green' : 'white'
      };
      return (
        <tr
          key={project.id}
          style={styles}
          onClick={() => this.onRowClicked(project)}
        >
          {this.renderTds(project)}
        </tr>
      );
    });

    return trs;
  }
  render() {
    return <tbody>{this.renderRows()}</tbody>;
  }
}

export default ProjectTableBody;
