import React from 'react';
import ProjectTableHeader from './projectTableHeader';
import projectData from './projects';
import ProjectAdd from './projectAdd';
import ProjectTableBody from './projectTableBody';
import ProjectSearch from './projectFilter';

export interface Project {
  [key: string]: string | boolean;
  id: string;
  name: string;
  description: string;
  owner: string;
  type: string;
}
interface IState {
  data: Project[];
  selectedProject: Project;
}

class ProjectTable extends React.Component<object, IState> {
  state: IState = {
    data: projectData,
    selectedProject: projectData[0]
  };

  onRowDeleteClicked = (
    mouseEvent: React.MouseEvent,
    clickedProject: Project
  ) => {
    mouseEvent.stopPropagation();
    this.setState({
      data: this.state.data.filter(project => project.id !== clickedProject.id)
    });
  };

  onRowClicked = (clickedProject: Project) => {
    this.setState({
      selectedProject: clickedProject
    });
  };

  onAddBtnClick = (sampleProject: Project) => {
    this.setState({
      data: [sampleProject, ...this.state.data]
    });
  };

  onProjectFilter = (typedValue: string) => {
    this.setState({
      data: projectData.filter(p => p.name.includes(typedValue))
    });
  };

  updateProjectType = (type: any, projectToUpdate: Project) => {
    this.setState(state => {
      return {
        data: state.data.map(p => {
          if (p.id === projectToUpdate.id) {
            return {
              ...p,
              type: type === 'scrum' ? 'kanban' : 'scrum'
            };
          }

          return {
            ...p
          };
        })
      };
    });
  };

  renderTable() {
    if (this.state.data.length === 0) {
      return <h2>No projects </h2>;
    } else {
      let keys = Object.keys(this.state.data[0]);
      return (
        <table className="table table-hover table-full table-striped">
          <ProjectTableHeader headerNames={keys} />
          <ProjectTableBody
            updateProjectType={this.updateProjectType}
            onRowClicked={this.onRowClicked}
            tableData={this.state.data}
            selectedProject={this.state.selectedProject}
            onRowDeleteClicked={this.onRowDeleteClicked}
          />
        </table>
      );
    }
  }

  render() {
    return (
      <>
        <ProjectSearch onProjectFilter={this.onProjectFilter} />
        <ProjectAdd onAddClick={this.onAddBtnClick} />
        <div className="row">
          <div className="col-lg-12">{this.renderTable()}</div>
        </div>
      </>
    );
  }
}

export default ProjectTable;
