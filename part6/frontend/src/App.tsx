import React from 'react';
import './App.css';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import ProjectTable from './project/projectTable';
import NavBar from './navbar';
import ProjectDetails from './project/projectDetails';

function User() {
  return <h1>User</h1>;
}
function Home() {
  return <h1>Home</h1>;
}

const App: React.FC = () => {
  return (
    <>
      <BrowserRouter>
        <NavBar />
        <Switch>
          <Redirect exact={true} from="/" to="/home" />
          <Route exact path="/project" component={ProjectTable} />
          <Route exact path="/project/:projectId" component={ProjectDetails} />
          <Route path="/user" component={User} />
          <Route path="/home" component={Home} />
        </Switch>
      </BrowserRouter>
    </>
  );
};

export default App;
