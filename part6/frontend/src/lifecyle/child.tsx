import React from 'react';

class Child extends React.PureComponent<{ count: any }> {
  constructor(props: any) {
    super(props);
    this.state = {};
    console.log('constructor of child');
  }

  static getDerivedStateFromProps() {
    console.log('get derived state from props of child called');
    return null;
  }

  shouldComponentUpdate(nextProps: any, nextstate: any) {
    console.log('should update called for child');
    return true;
  }

  componentDidUpdate() {
    console.log('did update of child');
  }

  componentDidMount() {
    console.log('component did mount of child called');
  }
  render() {
    console.log('render method of child called');
    return <h1>I am Child and count is {this.props.count}</h1>;
  }
}

export default Child;
