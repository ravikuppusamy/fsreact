import React from 'react';

class HoverCounter extends React.Component {
  render() {
    return (
      <button className="btn btn-primary" onMouseOver={this.props.updateCount}>
        The Hover Count is {this.props.count}
      </button>
    );
  }
}

export default HoverCounter;
