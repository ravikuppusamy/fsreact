import React from 'react';

interface MessageProps {
  message: string;
  color: string;
}
function Message(props: MessageProps) {
  return (
    <h2 style={{ color: props.color }} className="p-4">
      {props.message}
    </h2>
  );
}

export default Message;
