package com.digitall.fs.service;

import com.digitall.fs.model.Project;
import com.digitall.fs.model.ProjectStatus;
import com.digitall.fs.model.ProjectType;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectService {

    private static List<Project> projects = new ArrayList<>();

    static {

        projects.add(new Project("1", "stock ticker", "stock ticker app frontend in angular",
                "John Watson", ProjectType.scrum, "10-Sep-2019", ProjectStatus.in_progress));

        projects.add(new Project("2", "stock ticker api", "stock ticker app api in express.js",
                "Raja Sekar", ProjectType.scrum, "01-Jan-2019", ProjectStatus.in_progress));

        projects.add(new Project("3", "chartiq reports", "reports application in react and chartiq",
                "Preethi Chawla", ProjectType.kanban, "20-May-2019", ProjectStatus.in_progress));

        projects.add(new Project("4", "digitall fullstack java",
                "project management application in java and angular", "David",
                ProjectType.kanban, "10-Apr-2018", ProjectStatus.in_progress));

        projects.add(new Project("5", "digitall microservices",
                "project management application in microservices", "Priyanka",
                ProjectType.scrum, "23-Dec-2018", ProjectStatus.completed));


    }


    public List<Project> listProjects() {
        return projects;
    }


    public Project createProject(Project project) {
        String id = String.valueOf(projects.size() + 1);
        project.setId(id);
        project.setStatus(ProjectStatus.in_progress);
        projects.add(project);
        return project;
    }
}
