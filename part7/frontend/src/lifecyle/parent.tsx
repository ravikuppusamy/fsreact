import React from 'react';
import Child from './child';

class Parent extends React.Component<object, { count: any }> {
  constructor(props: any) {
    super(props);
    this.state = {
      count: 0
    };
    console.log('constructor of parent');
  }

  static getDerivedStateFromProps() {
    console.log('get derived state from props called');
    return null;
  }

  componentDidMount() {
    console.log('component did mount of parent called');
  }

  shouldComponentUpdate(nextProps: any, nextstate: any) {
    console.log('shoul update called for parent');
    return true;
  }

  componentDidUpdate() {
    console.log('did update of parent');
  }

  updateCount = () => {
    this.setState((state: any) => {
      return {
        count: state.count < 3 ? state.count + 1 : state.count
      };
    });
  };
  render() {
    console.log('render method of parent called');
    return (
      <span>
        <h1>I am Parent</h1>
        <button onClick={this.updateCount}>updateCount</button>
        <Child count={this.state.count} />
      </span>
    );
  }
}

export default Parent;
