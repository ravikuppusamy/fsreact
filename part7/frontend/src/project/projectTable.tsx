import React from 'react';
import ProjectTableHeader from './projectTableHeader';
import ProjectAdd from './projectAdd';
import ProjectTableBody from './projectTableBody';
import ProjectSearch from './projectFilter';
import { connect } from 'react-redux';
import { setProjectsAction, deleteProjectAction } from './state/actions';
import projects from './projects';

export interface Project {
  [key: string]: string | boolean;
  id: string;
  name: string;
  description: string;
  owner: string;
  type: string;
}

class ProjectTable extends React.Component<any, any> {
  onRowDeleteClicked = (
    mouseEvent: React.MouseEvent,
    clickedProject: Project
  ) => {
    mouseEvent.stopPropagation();
    this.props.deleteProject(clickedProject);
  };

  componentDidMount() {
    this.props.setProjects();
  }

  onRowClicked = (clickedProject: Project) => {};

  onAddBtnClick = (sampleProject: Project) => {};

  updateProjectType = (type: any, projectToUpdate: Project) => {};

  renderTable() {
    if (this.props.projects.length === 0) {
      return <h2>No projects </h2>;
    } else {
      let keys = Object.keys(this.props.projects[0]);
      return (
        <table className="table table-hover table-full table-striped">
          <ProjectTableHeader headerNames={keys} />
          <ProjectTableBody
            updateProjectType={this.updateProjectType}
            onRowClicked={this.onRowClicked}
            tableData={this.props.projects}
            onRowDeleteClicked={this.onRowDeleteClicked}
          />
        </table>
      );
    }
  }

  render() {
    return (
      <>
        <ProjectSearch />
        <ProjectAdd onAddClick={this.onAddBtnClick} />
        <div className="row">
          <div className="col-lg-12">{this.renderTable()}</div>
        </div>
      </>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    projects: state.projectState.projects
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    setProjects: () => dispatch(setProjectsAction(projects)),
    deleteProject: (prdl: Project) => dispatch(deleteProjectAction(prdl))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectTable);
