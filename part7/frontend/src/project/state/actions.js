export const SET_PROJECTS = 'SET_PROJECTS';
export const FETCH_PROJECTS = 'FETCH_PROJECTS';
export const DELETE_PROJECT = 'DELETE_PROJECT';
export const SEARCH_PROJECT = 'SEARCH_PROJECT';
export const CHANGE_SEARCH_TERM = 'CHANGE_SEARCH_TERM';

export const setProjectsAction = projects => {
  return {
    type: SET_PROJECTS,
    projects
  };
};
export const deleteProjectAction = projectToDelete => {
  return {
    type: DELETE_PROJECT,
    projectToDelete
  };
};

export const changeSearchTerm = term => {
  return {
    type: CHANGE_SEARCH_TERM,
    term
  };
};

export const searchProjectAction = termToSearch => {
  return {
    type: SEARCH_PROJECT,
    termToSearch
  };
};

export const fetchProjects = () => {
  return dispatch => {
    fetch(`http://localhost:8080/api/projects`)
      .then(res => res.json())
      .then(data => {
        dispatch(setProjectsAction(data));
      });
  };
};
