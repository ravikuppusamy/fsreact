import React from 'react';

interface HeaderProps {
  headerNames: string[];
}
class ProjectTableHeader extends React.Component<HeaderProps> {
  render() {
    const ths = this.props.headerNames.map(headerName => (
      <th key={headerName}>{headerName}</th>
    ));
    return (
      <thead>
        <tr>{ths}</tr>
      </thead>
    );
  }
}

export default ProjectTableHeader;
