class Point {
  private x: number;
  private y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  getX() {
    return this.x;
  }
  getY() {
    return this.y;
  }

  setX(x: number) {
    this.x = x;
    return this;
  }

  setY(y: number) {
    this.y = y;
    return this;
  }

  getDistance(p: Point): number {
    let xSquare = Math.pow(Math.abs(p.getX() - this.x), 2);
    let ySquare = Math.pow(Math.abs(p.getY() - this.y), 2);
    return Math.sqrt(ySquare + xSquare);
  }
}

export default Point;
