import React from 'react';
import ProjectTableHeader from './projectTableHeader';
import projectData from './projects';
import ProjectAdd from './projectAdd';
import ProjectTableBody from './projectTableBody';
import ProjectSearch from './projectFilter';
import ProjectDetails from './projectDetails';

export interface Project {
  [key: string]: string | boolean;
  id: string;
  name: string;
  description: string;
  owner: string;
  type: string;
}
interface IState {
  data: Project[];
  selectedProject: Project;
}

class ProjectTable extends React.Component<object, IState> {
  state: IState = {
    data: projectData,
    selectedProject: projectData[0]
  };

  onRowDeleteClicked = (
    mouseEvent: React.MouseEvent,
    clickedProject: Project
  ) => {
    mouseEvent.stopPropagation();
    this.setState({
      data: this.state.data.filter(project => project.id !== clickedProject.id)
    });
  };

  onRowClicked = (clickedProject: Project) => {
    this.setState({
      selectedProject: clickedProject
    });
  };

  onAddBtnClick = (sampleProject: Project) => {
    this.setState({
      data: [sampleProject, ...this.state.data]
    });
  };

  onProjectFilter = (typedValue: string) => {
    this.setState({
      data: projectData.filter(p => p.name.includes(typedValue))
    });
  };

  renderTable() {
    if (this.state.data.length === 0) {
      return <h2>No projects </h2>;
    } else {
      let keys = Object.keys(this.state.data[0]);
      return (
        <table className="table table-hover table-striped">
          <ProjectTableHeader headerNames={keys} />
          <ProjectTableBody
            onRowClicked={this.onRowClicked}
            tableData={this.state.data}
            selectedProject={this.state.selectedProject}
            onRowDeleteClicked={this.onRowDeleteClicked}
          />
        </table>
      );
    }
  }

  render() {
    return (
      <>
        <ProjectSearch onProjectFilter={this.onProjectFilter} />
        <ProjectAdd onAddClick={this.onAddBtnClick} />
        <div className="row">
          <div className="col-lg-6">{this.renderTable()}</div>
          <div className="colo-lg-6">
            <ProjectDetails project={this.state.selectedProject} />
          </div>
        </div>
      </>
    );
  }
}

export default ProjectTable;
