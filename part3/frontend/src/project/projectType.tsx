import React from 'react';
import scrumImage from '../assets//images/scrum.png';
import KanBanImage from '../assets//images/kanban.png';

interface Iprops {
  type: string | boolean;
  width: number;
  height: number;
}
const ProjectType = (props: Iprops) => {
  return props.type === 'scrum' ? (
    <img
      width={props.width}
      alt={'scrum'}
      height={props.height}
      src={scrumImage}
    />
  ) : (
    <img
      width={props.width}
      alt={'kanban'}
      height={props.height}
      src={KanBanImage}
    />
  );
};

export default ProjectType;
