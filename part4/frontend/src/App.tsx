import React from 'react';
import './App.css';
import ClickCounter from './hoc/click_counter';
import HoverCounter from './hoc/hover_counter';
import RenderCounter from './renderProp/renderCounter';

const App: React.FC = () => {
  return (
    <>
      <RenderCounter
        render={(count: any, update: any) => {
          return <HoverCounter count={count} updateCount={update} />;
        }}
      />

      <RenderCounter
        render={(count: any, update: any) => {
          return <ClickCounter count={count} updateCount={update} />;
        }}
      />
    </>
  );
};

export default App;
