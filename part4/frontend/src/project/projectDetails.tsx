import React from 'react';
import { Project } from './projectTable';
import ProjectType from './projectType';

interface Iprops {
  project: Project;
}
class ProjectDetails extends React.Component<Iprops> {
  render() {
    let project = this.props.project;
    return (
      <>
        <div className="p-3 border border-blue">
          <div className="row">
            <div className="col-lg-6 p-3">id : {project.id}</div>
            <div className="col-lg-6 p-3">name : {project.name}</div>{' '}
          </div>
          <div className="row">
            <div className="col-lg-6 p-3">
              description : {project.description}
            </div>
            <div className="col-lg-6 p-3">
              project type:{' '}
              <ProjectType
                width={50}
                height={50}
                type={project.type}
                onProjectTypeClicked={() => {}}
              />
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default ProjectDetails;
