import React from 'react';
import { Project } from './projectTable';

interface Iprops {
  onAddClick: (project: Project) => void;
}

class ProjectAdd extends React.Component<Iprops> {
  onAddBtnClicked = () => {
    let sampleProject = {
      id: Math.floor(Math.random() * 100 + 1).toString(),
      name: 'stock ticker',
      description: 'stock ticker app frontend in angular',
      owner: 'John Watson',
      type: 'scrum'
    };

    this.props.onAddClick(sampleProject);
  };
  render() {
    return (
      <div className="row">
        <div className="col-lg-2 m-3">
          <button onClick={this.onAddBtnClicked} className="btn btn-danger ">
            ADD PROJECT
          </button>
        </div>
      </div>
    );
  }
}

export default ProjectAdd;
