package com.digitall.fs.web.controller;

import com.digitall.fs.model.Project;
import com.digitall.fs.service.ProjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/projects")
@Slf4j
@CrossOrigin("http://localhost:3000")
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @GetMapping("")
    public List<Project> listProjects() {
        return this.projectService.listProjects();
    }

    @PostMapping("")
    @ResponseStatus(CREATED)
    public Project addProject(@RequestBody Project project) {
        return this.projectService.createProject(project);
    }
}
