import React from 'react';
import { connect } from 'react-redux';
import { setFormData } from './state/projectAddActions';
class ProjectForm extends React.Component<any> {
  handleInputChange = (event: any) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.props.setFormDataAction(name, value);
  };
  handleSubmit = (event: any) => {
    event.preventDefault();
    let newProject: any = {
      ...this.props.formData,
      id: Math.floor(Math.random() * 100 + 1)
    };
    this.props.onAddClick(newProject);
  };
  render() {
    return (
      <form className="border border-dark p-3" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            className="form-control"
            name="name"
            value={this.props.formData.name}
            onChange={this.handleInputChange}
            placeholder="Enter name of project"
          />
        </div>

        <div className="form-group">
          <label htmlFor="name">Description</label>
          <input
            type="text"
            className="form-control"
            name="description"
            value={this.props.formData.description}
            onChange={this.handleInputChange}
            placeholder="Enter description of project"
          />
        </div>

        <div className="form-group">
          <label htmlFor="name">Owner</label>
          <input
            type="text"
            className="form-control"
            name="owner"
            value={this.props.formData.owner}
            onChange={this.handleInputChange}
            placeholder="Enter owner of project"
          />
        </div>
        <div className="form-group">
          <label htmlFor="type">Select project type</label>
          <select
            className="form-control"
            name="type"
            value={this.props.formData.type}
            onChange={this.handleInputChange}
          >
            <option value="scrum">scrum</option>
            <option value="kanban">kanban</option>
          </select>
        </div>

        <input type="submit" className="btn btn-dark" value="Submit" />
      </form>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    formData: state.projectAddState.formData
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    setFormDataAction: (name: any, value: any) =>
      dispatch(setFormData(name, value))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectForm);
