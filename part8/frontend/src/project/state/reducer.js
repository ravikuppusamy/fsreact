import {
  SET_PROJECTS,
  DELETE_PROJECT,
  SEARCH_PROJECT,
  CHANGE_SEARCH_TERM,
  SHOW_SPINNER,
  HIDE_SPINNER
} from './actions';
import ALLPROJECTS from '../projects';

const initialState = {
  projects: [],
  filteredProjects: ALLPROJECTS,
  searchTerm: '',
  isSpinning: false
};

const filterProject = (state, action) => {
  return state.projects.filter(pr => {
    return pr.id !== action.projectToDelete.id;
  });
};
function projectReducer(state = initialState, action) {
  switch (action.type) {
    case DELETE_PROJECT: {
      return {
        ...state,
        projects: filterProject(state, action),
        filteredProjects: filterProject(state, action)
      };
    }
    case SET_PROJECTS: {
      return {
        ...state,
        projects: action.projects
      };
    }
    case SHOW_SPINNER: {
      return {
        ...state,
        isSpinning: true
      };
    }
    case HIDE_SPINNER: {
      return {
        ...state,
        isSpinning: false
      };
    }
    case SEARCH_PROJECT: {
      return {
        ...state,
        projects: state.filteredProjects.filter(pr => {
          return pr.name.includes(action.termToSearch);
        })
      };
    }
    case CHANGE_SEARCH_TERM: {
      return {
        ...state,
        searchTerm: action.term,
        projects: state.filteredProjects.filter(pr => {
          return pr.name.includes(action.term);
        })
      };
    }

    default:
      return state;
  }
}

export default projectReducer;
