export const SET_FORM_DATA = 'SET_FORM_DATA';

export const setFormData = (name, value) => {
  return {
    type: SET_FORM_DATA,
    name,
    value
  };
};
