import Point from './point';
class Circle {
  constructor(private radius: number, private center: Point) {}

  getRadius(): number {
    return this.radius;
  }

  getCenterPoint(): Point {
    return this.center;
  }

  getCircle(): Circle {
    return this;
  }

  isIntersectingCircle(c: Circle): boolean {
    const distanceBetweenCentres = Math.abs(
      c.getCenterPoint().getDistance(this.center)
    );
    const radiiSum = Math.abs(this.getRadius() + c.getRadius());
    return radiiSum > distanceBetweenCentres;
  }
}

export default Circle;
