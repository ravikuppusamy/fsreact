import Circle from './circle';
import Point from './point';

describe('Circle', () => {
  it('should be able to create Circle', () => {
    const center: Point = new Point(2, 2);
    const c: Circle = new Circle(5, center);
    expect(c).toBeDefined();
  });
  it('should return true if intersecting with another Circle', () => {
    const center1: Point = new Point(2, 2);
    const c1: Circle = new Circle(1, center1);

    const center2: Point = new Point(3, 2);
    const c2: Circle = new Circle(1, center2);
    expect(c1.isIntersectingCircle(c2)).toEqual(true);
  });
  it('should return if not intersecting with another Circle', () => {
    const center1: Point = new Point(2, 2);
    const c1: Circle = new Circle(1, center1);

    const center2: Point = new Point(5, 2);
    const c2: Circle = new Circle(1, center2);
    expect(c1.isIntersectingCircle(c2)).toEqual(false);
  });
});
