import Point from './point';

describe('Point', function() {
  it('should be able to create point', () => {
    const p: Point = new Point(2, 3);
    expect(p).toBeDefined();
  });
  it('should be able to get x', () => {
    const p: Point = new Point(2, 3);
    expect(p.getX()).toEqual(2);
  });
  it('should be able to get y', () => {
    const p: Point = new Point(2, 3);
    expect(p.getY()).toEqual(3);
  });
  it('should be able to get distance from another point', () => {
    const p: Point = new Point(2, 3);
    const q: Point = new Point(5, 3);
    expect(p.getDistance(q)).toEqual(3);
  });
});
