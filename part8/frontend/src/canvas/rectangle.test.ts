import Rectange from './rectangle';
import Point from './point';

describe('Reactangle', () => {
  it('should be able to create a rectangle', () => {
    const r: Rectange = new Rectange([
      new Point(0, 2),
      new Point(2, 2),
      new Point(2, 0),
      new Point(0, 0)
    ]);

    expect(r).toBeDefined();
  });

  it('should return true   if  intersection exists', () => {
    const r: Rectange = new Rectange([
      new Point(0, 2),
      new Point(2, 2),
      new Point(2, 0),
      new Point(0, 0)
    ]);

    const p: Point = new Point(1, 1);

    expect(r.isPointInside(p)).toEqual(true);
  });

  it('should return false  if intersection does not exists', () => {
    const r: Rectange = new Rectange([
      new Point(0, 2),
      new Point(2, 2),
      new Point(2, 0),
      new Point(0, 0)
    ]);

    const p: Point = new Point(3, 1);

    expect(r.isPointInside(p)).toEqual(false);
  });
});
