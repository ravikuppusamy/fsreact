import React from "react";
import { render } from "@testing-library/react";
import Sample from "./sample";
it("renders sample component", () => {
  const { getByText } = render(<Sample />);
  expect(getByText("sample for testing")).toBeInTheDocument();
});
