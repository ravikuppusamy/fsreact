import { createStore, combineReducers, applyMiddleware } from 'redux';
import projectReducer from './project/state/reducer';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import projectAddReducer from './project/state/projectAddReducer';
const rootReducer = combineReducers({
  projectState: projectReducer,
  projectAddState: projectAddReducer
});

const store = createStore(rootReducer, applyMiddleware(thunk, logger));

export default store;
