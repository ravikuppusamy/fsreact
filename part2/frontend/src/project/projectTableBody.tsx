import React from 'react';
import { Project } from './projectTable';
import ProjectType from './projectType';

interface IProps {
  tableData: Project[];
  onRowDeleteClicked: (e: React.MouseEvent, project: Project) => void;
}
class ProjectTableBody extends React.Component<IProps> {
  renderTds(project: Project) {
    var keys = Object.keys(project);
    const tds = keys.map((key: any) => {
      if (key == 'type') {
        return (
          <td key={key}>
            <ProjectType width={50} height={50} type={project[key]} />
          </td>
        );
      }
      return <td key={key}>{project[`${key}`]}</td>;
    });

    return [
      ...tds,
      <td key={project.id + 'del'}>
        <button
          onClick={(e: React.MouseEvent) =>
            this.props.onRowDeleteClicked(e, project)
          }
          className="btn btn-danger"
        >
          DELETE
        </button>
      </td>
    ];
  }
  renderRows() {
    const trs = this.props.tableData.map(project => {
      return <tr key={project.id}>{this.renderTds(project)}</tr>;
    });

    return trs;
  }
  render() {
    return <tbody>{this.renderRows()}</tbody>;
  }
}

export default ProjectTableBody;
