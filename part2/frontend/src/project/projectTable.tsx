import React from 'react';
import ProjectTableHeader from './projectTableHeader';
import projectData from './projects';
import ProjectAdd from './projectAdd';
import ProjectTableBody from './projectTableBody';

export interface Project {
  [key: string]: string | boolean;
  id: string;
  name: string;
  description: string;
  owner: string;
  type: string;
}
interface IState {
  data: Project[];
}

class ProjectTable extends React.Component<object, IState> {
  state: IState = {
    data: projectData
  };

  onRowDeleteClicked = (
    mouseEvent: React.MouseEvent,
    clickedProject: Project
  ) => {
    mouseEvent.stopPropagation();
    this.setState({
      data: this.state.data.filter(project => project.id !== clickedProject.id)
    });
  };

  onRowClicked = (clickedProject: Project) => {
    this.setState({
      data: projectData
    });
  };

  onAddBtnClick = (sampleProject: Project) => {
    this.setState({
      data: [sampleProject, ...this.state.data]
    });
  };

  render() {
    let keys = Object.keys(this.state.data[0]);
    return (
      <>
        <ProjectAdd onAddClick={this.onAddBtnClick} />
        <div className="row">
          <div className="col-lg-12">
            <table className="table table-hover table-striped">
              <ProjectTableHeader headerNames={keys} />
              <ProjectTableBody
                tableData={this.state.data}
                onRowDeleteClicked={this.onRowDeleteClicked}
              />
            </table>
          </div>
        </div>
      </>
    );
  }
}

export default ProjectTable;
